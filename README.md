# README #

This is a repo with T-Systems Java School preliminary examination tasks.
Code points where you solution is to be located are marked with TODOs.

The solution is to be written using Java 1.8 only, external libraries are forbidden. 
You can add dependencies with scope "test" if it's needed to write new unit-tests.

The exam includes 3 tasks to be done: [Calculator](/tasks/Calculator.md), [Pyramid](/tasks/Pyramid.md), and 
[Subsequence](/tasks/Subsequence.md)

### Result ###

* Author name : Vladislav Guryanov
* Codeship : [ ![Codeship Status for tschool/javaschoolexam](https://app.codeship.com/projects/47438600-9a73-0136-e904-5e8cc09e6238/status?branch=master)](https://app.codeship.com/projects/305657)
