package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null)
            throw new IllegalArgumentException();

        int matchesNeeded = x.size();
        for (int xInd = 0, yInd = 0; xInd < x.size() && yInd < y.size() && matchesNeeded > 0; ) {
            if (x.get(xInd).equals(y.get(yInd))) {
                matchesNeeded--;
                xInd++;
                yInd++;
            } else
                yInd++;
        }
        return matchesNeeded == 0;
    }
}
