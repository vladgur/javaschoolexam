package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {
    private static final int FILLER = 0;

    /**
     * Builds a pyramid with sorted values (with minimum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int height = getHeight(inputNumbers.size());
        if (height < 1 || inputNumbers.contains(null))
            throw new CannotBuildPyramidException();
        int width = height * 2 - 1;
        int inpIndex = 0;
        int[][] result = new int[height][width];
        inputNumbers.sort(Integer::compareTo);

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if ((i + j + 1) % 2 == height % 2
                        && Math.abs(width / 2 - j) < i + 1) {
                    result[i][j] = inputNumbers.get(inpIndex);
                    inpIndex++;
                } else
                    result[i][j] = FILLER;
            }
        }

        return result;
    }

    private int getHeight(int i) {
        int j = 0;
        while (j <= i) {
            i -= j;
            if (i == 0) break;
            j++;
        }
        if (i != 0)
            j = -1;
        return j;
    }
}
