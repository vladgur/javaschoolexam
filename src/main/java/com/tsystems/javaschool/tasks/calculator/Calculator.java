package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Locale;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Calculator {
    private static final char
            PLUS_SIGN = '+',
            MINUS_SIGN = '-',
            MULTIPLICATION_SIGN = '*',
            DIVISION_SIGN = '/',
            OP_BRACKET = '(',
            CL_BRACKET = ')';
    private static final String
            ALLOWED_OPERATOR_PATTERN = "[+-//*///]",
            OUTPUT_FORMAT_PATTERN = "#.####";

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty())
            return null;

        LinkedList<Double> operands = new LinkedList<>();
        LinkedList<Character> operators = new LinkedList<>();

        for (int i = 0; i < statement.length(); i++) {
            char c = statement.charAt(i);
            if (c == OP_BRACKET)
                operators.add(OP_BRACKET);
            else if (c == CL_BRACKET) {
                try {
                    while (operators.getLast() != OP_BRACKET) {
                        if (!executeOperation(operands, operators.removeLast()))
                            return null;
                    }
                    operators.removeLast();
                } catch (NoSuchElementException e) {
                    return null;
                }
            } else if (String.valueOf(c).matches(ALLOWED_OPERATOR_PATTERN)) {
                try {
                    while (!operators.isEmpty() && getOperatorPriority(operators.getLast()) >= getOperatorPriority(c)) {
                        if (!executeOperation(operands, operators.removeLast()))
                            return null;
                    }
                    operators.add(c);
                } catch (NoSuchElementException e) {
                    return null;
                }
            } else {
                StringBuilder operand = new StringBuilder();
                if (statement.charAt(i) == MINUS_SIGN || statement.charAt(i) == PLUS_SIGN)
                    operand.append(statement.charAt(i++));
                while (i < statement.length() && (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.'))
                    operand.append(statement.charAt(i++));
                --i;
                try {
                    operands.add(Double.parseDouble(operand.toString()));
                } catch (NumberFormatException e) {
                    return null;
                }
            }
        }

        try {
            while (!operators.isEmpty())
                if (!executeOperation(operands, operators.removeLast()))
                    return null;
        } catch (NoSuchElementException e) {
            return null;
        }

        return roundNumber(operands.get(0));
    }

    private static boolean executeOperation(LinkedList<Double> operands, char operation) {
        double rightOp = operands.removeLast();
        double leftOp = operands.removeLast();
        Double result = null;

        switch (operation) {
            case PLUS_SIGN:
                result = leftOp + rightOp;
                break;
            case MINUS_SIGN:
                result = leftOp - rightOp;
                break;
            case MULTIPLICATION_SIGN:
                result = leftOp * rightOp;
                break;
            case DIVISION_SIGN:
                if (rightOp == 0)
                    result = null;
                else
                    result = leftOp / rightOp;
                break;
        }

        operands.add(result);
        return result != null;
    }

    private static int getOperatorPriority(char operator) {
        switch (operator) {
            case PLUS_SIGN:
            case MINUS_SIGN:
                return 1;
            case MULTIPLICATION_SIGN:
            case DIVISION_SIGN:
                return 2;
        }
        return -1;
    }

    private static String roundNumber(double number) {
        DecimalFormat decimalFormat = new DecimalFormat(OUTPUT_FORMAT_PATTERN, DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        return decimalFormat.format(number);
    }
}
